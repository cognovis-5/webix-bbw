import { JetView } from "webix-jet";
import LoginForm from "./login-form";
import RegistrationForm from "./registration-form";
import { OpenAPI } from "../openapi/index";
  
export default class MainView extends JetView {

    
    config():webix.ui.layoutConfig {
        const localStorageCredentials = webix.storage.local.get("wjet_user");
        const bearerToken = webix.storage.local.get("cognovis_bearer_token");
        OpenAPI.TOKEN = bearerToken;

        if (localStorageCredentials !== null) {
            const layout = {
                view:"scrollview",
                body:{
                    autoheight:true,
                    height:200,
                    rows:[
                        RegistrationForm
                    ]
                }
            };
            return layout      
        } else {
            const layout = {
                view:"layout",
                rows: [
                    LoginForm
                ]
            };
            return layout    
        } 
    }

}