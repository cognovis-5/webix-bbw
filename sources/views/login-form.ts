import { WebixHelpers } from "../../sources/modules/webix-helpers/webix-helpers";
import { CognovisPleaseWaitWindow } from "../../sources/services/cognovis-please-wait-window";
import { JetView } from "webix-jet";
import { container } from "tsyringe";
import { CognovisRestSystemService, IntranetCustBbwRestService } from "../../sources/openapi";


export default class LoginForm extends JetView {

    idPrefix = "bbwLf";
    webixHelpers:WebixHelpers;
    cognovisPleaseWaitWindow: CognovisPleaseWaitWindow;

    config():webix.ui.layoutConfig {
        const layout = {
            view:"form",
            localId:`${this.idPrefix}LoginForm`,
            padding:20,
            rows:[
                {
                    view:"layout",
                    cols:[{
                        view:"text",
                        name:"email",
                        label:"E-Mail",
                        labelWidth:160
                    }]
                },
                {
                    view:"layout",
                    cols:[{
                        view:"text",
                        name:"first_names",
                        label:"First Name(s)",
                        labelWidth:160
                    }]
                },
                {
                    view:"layout",
                    cols:[{
                        view:"text",
                        name:"last_name",
                        label:"Last Name",
                        labelWidth:160
                    }]
                },
                {
                    view:"button",
                    value:"Submit",
                    click:() => {
                        this.submitForm();
                    }
                }
            ],
            rules: {
                email:webix.rules.isEmail,
            }
        };
        return layout
    }

    getLoginForm() {
        const field = {
            view:"text",
            name:"email",
            label:"E-Mail",
            labelWidth:160,
            on: {
                onEnter:() => {
                    this.submitForm();
                }
            }
        };
        const layout = {
            view:"layout",
            rows:[
                field
            ]
        }
        return layout
    }

    submitForm():void {
        this.cognovisPleaseWaitWindow.show({ message: "Please_wait"});
        const form = this.$$(`${this.idPrefix}LoginForm`) as webix.ui.form;
        const values = form.getValues();
        if(form.validate()) {
            const values = form.getValues();
            IntranetCustBbwRestService.postRegisterUser({
                email:values.email,
                firstNames: values.first_names,
                lastName: values.last_name
            })
            .then(res => {
                if (res.user_id != undefined) {
                    webix.storage.local.put("wjet_user", {
                        user_id: res.user_id,
                        token: res.token,
                        site_wide_admin: 0,
                        name: values.email,
                    });
                    webix.storage.local.put(
                        "cognovis_bearer_token",
                        res.bearer_token
                    );
                    // Reset local translations
                    webix.storage.local.remove("cognovis_translations");
                    location.reload();
                } else {
                    this.showErrorAlert("Can't authorize", "Username or password is wrong");
                }
            });
        }
        this.cognovisPleaseWaitWindow.hide();
    }

    init():void {
        this.webixHelpers = container.resolve(WebixHelpers);
        this.cognovisPleaseWaitWindow = container.resolve(CognovisPleaseWaitWindow);
    }

    showErrorAlert(title:string, error:string):void {
        webix.alert({
            type:"error",
            title:title,
            text:error,
            position:"center"
        })
    }
}