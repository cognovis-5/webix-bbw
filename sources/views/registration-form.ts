import { CognovisRestCompanyService, CognovisRestService, CognovisRestPricingService, IBbwRegistration, IntranetCustBbwRestService, IntegerInt32 } from "../openapi/index";
import { JetView } from "webix-jet";
import { container } from "tsyringe";
import { CognovisPleaseWaitWindow } from "../services/cognovis-please-wait-window";
import { config } from "../config";

export default class RegistrationForm extends JetView {

    idPrefix = "bbwRf";
    cognovisPleaseWaitWindow: CognovisPleaseWaitWindow;

    config():webix.ui.layoutConfig {
        console.log(this.app)
        const layout = {
            view:"form",
            scroll:"y",
            localId:`${this.idPrefix}NewEntryForm`,
            padding:20,
            rows:[
                {
                    view:"flexlayout",
                    gravity:1,
                    cols:[
                        {
                            view:"layout",
                            minWidth:350,
                            rows:[
                                this.getContactInformationFieldset(),
                                this.getAttendanceInformationFieldset(),
                            ]
                        },
                        this.getVerticalSpacer(),
                        {
                            view:"layout",
                            minWidth:350,
                            rows:[
                                this.getOtherInformationFieldset()
                            ]
                        },
                        this.getVerticalSpacer(),
                    ],
                },
                {
                    view:"button",
                    css:"submit-registration-button",
                    value:"Submit",
                    name:"submit",
                    click:() => {
                        this.submitForm();
                    }
                }
            ],
            rules: {
/*                email:webix.rules.isEmail,
                first_names:webix.rules.isNotEmpty,
                last_name:webix.rules.isNotEmpty,*/
                ha_line1:webix.rules.isNotEmpty,
                ha_city:webix.rules.isNotEmpty,
                ha_postal_code:webix.rules.isNotEmpty,
                ha_country_code:webix.rules.isNotEmpty,
                event_participant_type_id:webix.rules.isNotEmpty,
                course_material_id:webix.rules.isNotEmpty,
                membership: webix.rules.isChecked,
                terms_and_conditions: webix.rules.isChecked,
                legal_information:webix.rules.isChecked
            }
        };
        return layout
    }

    getBasicInformationFieldset():webix.ui.fieldsetConfig {
        const fieldset = {
            view:"fieldset",
            paddingY:60,
            label:"Basic Information",
            body:{
                rows:[
                    this.getTextField("email", "E-Mail"),
                    this.getTextField("first_names", "First Names"),
                    this.getTextField("last_name", "Last Name"),
                ]
            }
        } as unknown as webix.ui.fieldsetConfig;
        return fieldset
    }

    getContactInformationFieldset():webix.ui.fieldsetConfig {
        const fieldset = {
            view:"fieldset",
            paddingY:60,
            label:"Contact Information",
            body: {
                rows:[
                    this.getTextField("ha_line1", "Address"),
                    this.getTextField("ha_city", "City"),
                    this.getTextField("ha_state", "State"),    
                    this.getTextField("ha_postal_code", "Postal Code"),    
                    this.getCountryComboField("ha_country_code", "Country"),               
                ]
            }

        } as unknown as webix.ui.fieldsetConfig;
        return fieldset
    }

    getAttendanceInformationFieldset():webix.ui.fieldsetConfig {
        const fieldset = {
            view:"fieldset",
            paddingY:60,
            label:"Attendance",
            body: {
                rows:[
                    this.getMaterialsComboField("course_material_id", "Attendance"),
                    this.getHoritontalSpacer(),
                    this.getCategoryComboField("event_participant_level_id","Dance Level","Event Participant Level", "Click <a href='https://www.berlinbalboaweekend.de/' target='_blank'>www.berlinbalboaweekend.de</a> to read details about the levels in a new window</div>"),
                    this.getTextField("comments", "Comments","Please provide a comment why you chose the dance level. Especially if it differs between Shag & Balboa")
                ]
            }

        } as unknown as webix.ui.fieldsetConfig;
        return fieldset
    }

    getOtherInformationFieldset():webix.ui.fieldsetConfig {
        const fieldset = {
            view:"fieldset",
            paddingY:60,
            label:"Other Information",
            body: {
                rows:[
                    this.getCategoryComboField("event_participant_type_id", "Dance Role", "Dance Role", "The role you dance most often socially"),
                    this.getHoritontalSpacer(),
                    this.getTextField("event_partners_text", "Dance Partner", "Please provide the E-MAIL address your dance partner registered with. Futhermore, if you sign up with a dance partner you must register in opposite roles. We will consider your applications together, you need NOT have the same amount of dance experience as your partner."),      
                    this.getHoritontalSpacer(),
                    this.getCheckboxField("membership", "Verein Membership", "To participate in this workshop every participant must become a supporting member of Balboa in Berlin e.V.. As a Foerdermitglied (supporting member) you will have no obligations or votings rights with respect to the activites of Balboa in Berlin e.V.."),
                    this.getHoritontalSpacer(),
                    this.getCheckboxField("terms_and_conditions", "Terms And Conditions", "Click <a href='https://www.berlinbalboaweekend.de/impressum' target='_blank'>www.berlinbalboaweekend.de/terms</a> to read details in a new window</div>"),
                    this.getHoritontalSpacer(),
                    this.getCheckboxField("legal_information", "I  have duly noted the above legal information and I agree on the processing and storage of my personal data as described.", "Click <a href='https://www.berlinbalboaweekend.de/impressum' target='_blank'>www.berlinbalboaweekend.de/impressum</a> to read details in a new window.We collect, store and use your personal data only in accordance with the content of our privacy policy and the applicable data protection regulations, in particular the European General Data Protection Regulation (DSGVO) and the national data protection regulations.")
                ]
            }

        } as unknown as webix.ui.fieldsetConfig;
        return fieldset
    }

    getTextField(name:string, label:string, additionalInfo?:string) {
        const field = {
            view:"text",
            name:name,
            label:label,
            labelWidth:120
        };
        const layout = {
            view:"layout",
            autoheight:true,
            rows:[
                field
            ]
        }
        if(additionalInfo) {
            const infoField:webix.ui.layoutConfig = this.getFieldAdditionlInfo(additionalInfo);
            layout.rows.push(infoField as { view: string; invalidMessage:string; name: string; label: string; labelWidth: number; });
        }
        return layout
    }

    getCheckboxField(name:string, label:string, additionalInfo:string):webix.ui.layoutConfig {
        const checkbox = {
            view:"checkbox",
            name:name,
            required:true,
            labelRight:label,
            invalidMessage: "Make sure to mark that checkbox",
            labelWidth:0
        };
        const layout = {
            view:"layout",
            autoheight:true,
            rows:[
                checkbox,
                this.getFieldAdditionlInfo(additionalInfo)
            ]
        };
        return layout
    }

    getFieldAdditionlInfo(info:string):webix.ui.layoutConfig {
        const additionalInfoField = {
            view:"layout",
            autoheight:true,
            height:0,
            borderless:true,
            css: {
                "margin-left":"-5px !important;",
            },
            cols:[
                {
                    template:'<i class="fa fa-info-circle" aria-hidden="true"></i>',
                    width:28,
                    css:{
                        "color":"#5FA2DD",
                        "margin-top":"1px !important"
                    },
                    borderless:true,
                },
                {
                    template:`<span style='font-size:0.71rem; line-height:15px;'>${info}</span>`,
                    autoheight:true,
                    borderless:true,
                }
            ]
        };
        return additionalInfoField
    }

    getCountryComboField(name:string, label:string, additionalInfo?:string):webix.ui.layoutConfig {
        const combo = {
            view:"combo",
            name:name,
            label:label,
            labelWidth:120,
            suggest: {
                body: {
                    url: () => {
                        return CognovisRestCompanyService.getCountries()
                        .then(counries => {
                            const preparedForCombo = counries.map(country => ({id:country.iso, value:country.country_name}));
                            return preparedForCombo
                        });
                    }
                },
            },
        };
        const layout = {
            view:"layout",
            autoheight:true,
            rows:[
                combo
            ]
        };
        if(additionalInfo) {
            const infoField:webix.ui.layoutConfig = this.getFieldAdditionlInfo(additionalInfo);
            layout.rows.push(infoField as any);
        }
        return layout
    }

    getMaterialsComboField(name:string, label:string, additionalInfo?:string):webix.ui.layoutConfig {
        const combo = {
            view:"combo",
            name:name,
            label:label,
            labelWidth:120,
            suggest: {
                body: {
                    url: () => {
                        return CognovisRestPricingService.getMaterial({
                            materialTypeId:9004
                        })
                        .then(materials => {
                            const preparedForCombo = materials.map(material => ({id:material.material.id, value:material.material.name}));
                            return preparedForCombo
                        });
                    }
                },
            },
        };
        const layout = {
            view:"layout",
            rows:[
                combo
            ]
        };
        return layout
    }

    getCategoryComboField(name:string, label:string, categoryType:string, additionalInfo?:string):webix.ui.layoutConfig {
        const combo = {
            view:"combo",
            name:name,
            label:label,
            labelWidth:120,
            suggest: {
                body: {
                    url: () => {
                        return CognovisRestService.getImCategory(
                            {categoryType:categoryType}
                        )
                        .then(categories => {
                            const preparedForCombo = categories.map(category => ({id:category.category_id, value:category.category_translated}));
                            return preparedForCombo
                        });
                    }
                },
            },
        };
        const layout = {
            view:"layout",
            autoheight:true,
            height:0,
            rows:[
                combo
            ]
        };
        if(additionalInfo) {
            const infoField:webix.ui.layoutConfig = this.getFieldAdditionlInfo(additionalInfo);
            layout.rows.push(infoField as any);
        }
        return layout
    }

    getVerticalSpacer():webix.ui.spacerConfig {
        const spacer = {
            view:"spacer",
            width:20
        };
        return spacer
    }

    getHoritontalSpacer():webix.ui.spacerConfig {
        const spacer = {
            view:"spacer",
            height:20
        }
        return spacer
    }

    submitForm():void {
        const form = this.$$(`${this.idPrefix}NewEntryForm`) as webix.ui.form;
        const element = form.queryView({name:"submit"});
        const elementById = webix.$$(element.config.id) as webix.ui.text;
        if (elementById.config.value == "Logout") {
            webix.storage.local.remove("wjet_user");            
            webix.storage.local.remove("cognovis_participant_id");            
            webix.storage.local.remove("current_user_profiles");
            webix.storage.local.remove("cognovis_bearer_token");
            webix.storage.session.remove("current_user_profiles");
            webix.storage.session.remove("cached_cognovis_categories");
            webix.storage.local.remove("cognovis_translations");
            window.location.reload();

        } else {
            if(form.validate()) {
                // this.cognovisPleaseWaitWindow.show({ message: "Please_wait"});
                const values = form.getValues();
                IntranetCustBbwRestService.postBbwRegistration({
                    requestBody:{
                        project_id:config.projectId,
    /*                    email:values.email,
                        first_names:values.first_names,
                        last_name:values.last_name,*/
                        ha_line1:values.ha_line1,
                        ha_city:values.ha_city,
                        ha_country_code:values.ha_country_code,
                        ha_postal_code:values.ha_postal_code,
                        ha_state:values.state,
                        event_participant_type_id:values.event_participant_type_id,
                        event_partners_text:values.event_partners_text,
                        course_material_id:values.course_material_id,
                        event_participant_level_id:values.event_participant_level_id
                    }
                })
                .then(res => {
                    //this.cognovisPleaseWaitWindow.hide();
                    if(res.participant_id) {
                        this.injectExistingParticipant(res);
                        // webix.storage.local.put("cognovis_participant_id", res.participant_id);
                        webix.alert('Thank you for registering we look forward to see you. Data is correctly stored. For data privacy reasons you will not see details')
                        // window.location.reload();
                    }
                })
                .catch(() => {
                // this.cognovisPleaseWaitWindow.hide();
                })
                .finally(() => {
                // this.cognovisPleaseWaitWindow.hide();
                });
            }
        }        
    }

    injectExistingParticipant(registrationData:IBbwRegistration):void {
        const form = this.$$(`${this.idPrefix}NewEntryForm`) as webix.ui.form;
        const values = Object.keys(form.elements)
        values.map(input => {
            const element = form.queryView({name:input});
            if(element) {
                const elementById = webix.$$(element.config.id) as webix.ui.text;
                if(input == "submit") {
                    elementById.define("value", "Logout")
                    elementById.refresh();
                } else {
        
                    if(elementById) {
                        if(elementById.config.view === "checkbox") {
                            (elementById as unknown as webix.ui.checkbox).toggle();
                            (elementById as unknown as webix.ui.checkbox).disable();
                        } else {
                            elementById.setValue(registrationData[input]);
                        }
                        elementById.define("disabled", true);
                        elementById.refresh();
                    }
                }
            }
        });
        const participantField = form.queryView({name:"event_participant_type_id"}) as webix.ui.combo;
        participantField.setValue(registrationData.event_participant_type.id.toString());
        const levelField = form.queryView({name:"event_participant_level_id"}) as webix.ui.combo;
        levelField.setValue(registrationData.event_participant_level.id.toString());
    }

    init():void {
        const localStorageCredentials = webix.storage.local.get("wjet_user");

        IntranetCustBbwRestService.getBbwRegistration({
            projectId:config.projectId,
            email: localStorageCredentials.name            
        })
        .then(existingParticipantData => {
            this.injectExistingParticipant(existingParticipantData);
            //this.cognovisPleaseWaitWindow.hide();
        })
        .catch(() => {
            //this.cognovisPleaseWaitWindow.hide();
        });
    }
}