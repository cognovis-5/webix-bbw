if (window._env_ == undefined) {
    window._env_ = require('../env.json');
} 

const serverUrl = window._env_.SERVER_URL;
const portalUrl = window._env_.PORTAL_URL;
const logoUrl = window._env_.LOGO_URL;
const webixPackage = window._env_.WEBIX_PORTAL_PATH;

export const config = {
    serverUrl: serverUrl,
    portalUrl: portalUrl,
    logoUrl: logoUrl,
    restUrl: serverUrl + "/cognovis-rest/",
    webixUrl: serverUrl + webixPackage,
    projectId:64226,
    defaultContactAgainDays:2,
    maxUploadFileSize:150000000, //150 MB,
};
