import "./styles/app.css";
import {JetApp, EmptyRouter, plugins, HashRouter } from "webix-jet";
import "reflect-metadata";
import { stringify } from "@xbs/webix-pro";

declare const APPNAME;
declare const VERSION;
declare const PRODUCTION;
declare const BUILD_AS_MODULE;
declare const BASE_URL;
export default class MyApp extends JetApp{
	constructor(config){
		const defaults = {
			id 		: APPNAME,
			version : VERSION,
			router 	: BUILD_AS_MODULE ? EmptyRouter : HashRouter,
			debug 	: !PRODUCTION,
            start 	: "main",
            views: {
                "register": "main/register",
            }
		};
        super({ ...defaults, ...config });
        //this.use(plugins.User, { model: session });
    }
    
}

if (!BUILD_AS_MODULE){

	webix.ready(() => {
        const myApp = new MyApp({});
        webix.ui.fullScreen();
        webix.Touch.disable();
        myApp.render();
    });
}