/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export enum IntranetAbsenceType {
    VACATION = 5000,
    PERSONAL = 5001,
    SICK = 5002,
    TRAVEL = 5003,
    TRAINING = 5004,
    BANK_HOLIDAY = 5005,
    OVERTIME = 5006,
    REDUCTION_IN_WORKING_HOURS = 5007,
    WEEKEND = 5009,
}
