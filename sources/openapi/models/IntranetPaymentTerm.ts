/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export enum IntranetPaymentTerm {
    N14_DAYS = 80114,
    N30_DAYS = 80130,
    N60_DAYS = 80160,
}
