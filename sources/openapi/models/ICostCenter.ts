/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { ICategoryObject } from './ICategoryObject';
import type { INamedId } from './INamedId';

export type ICostCenter = {
    /**
     * Cost center id and name
     */
    cost_center?: INamedId;
    /**
     * Cost center parent if it exists
     */
    parent?: INamedId;
    /**
     * Status of the cost center
     */
    cost_center_status?: ICategoryObject;
    /**
     * What kind of cost center are we looking at
     */
    cost_center_type?: ICategoryObject;
    /**
     * Is this a department?
     */
    department_p?: boolean;
    /**
     * Person managing the cost center, typically for the department
     */
    manager?: INamedId;
    /**
     * Description of the cost center
     */
    description?: string;
};

