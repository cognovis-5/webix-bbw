/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { IDynamicSubtype } from './IDynamicSubtype';
import type { IIdValue } from './IIdValue';
import type { INamedId } from './INamedId';

export type IDynamicAttribute = {
    /**
     * Attribute we should display/edit - attribute.name reflects the most likely name used in the endpoints. For skills it is the column in the table where we have a match to (e.g. source language skill matches to source_language_id as this is denormalized).
     */
    attribute?: INamedId;
    /**
     * Name to use for displaying the attribute
     */
    display_name?: string;
    /**
     * Key with the i18n for this attribute
     */
    i18n_key?: string;
    /**
     * What type of attribute do we have (acs_attributes / im_skills)
     */
    attribute_type?: string;
    /**
     * Information about the subtype dependent data
     */
    dynamic_subtypes?: Array<IDynamicSubtype>;
    /**
     * Widget object
     */
    widget?: INamedId;
    /**
     * Type of widget to use - frontend needs to figure out what to do with it. Special cases are skills and categories
     */
    widget_type?: string;
    /**
     * If filled, have the frontend get the data from CognovisCategory.getCategory - provided both for skills as well as categories
     */
    category_type?: string;
    /**
     * Array of id + name objects. Might contain additional information. Not used for categories or skills
     */
    widget_data?: Array<IIdValue>;
    /**
     * Y position of dynfield (order)
     */
    sort_order?: number;
};

