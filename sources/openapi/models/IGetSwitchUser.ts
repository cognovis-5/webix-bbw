/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type IGetSwitchUser = {
    /**
     * UserID of the user in case of success
     */
    user_id?: number;
    /**
     * Token of the user_id requested
     */
    token?: string;
    /**
     * token used now instead of user_id+token.
     */
    bearer_token?: string;
};

