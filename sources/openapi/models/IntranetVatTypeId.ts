/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { IntranetVatType } from './IntranetVatType';

export type IntranetVatTypeId = IntranetVatType;
