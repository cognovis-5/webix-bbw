/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export enum IntranetMaterialType {
    COURSE = 9004,
    FOOD_CHOICE = 9007,
    CUSTOMER_MATERIAL = 9020,
    PROVIDER_MATERIAL = 9022,
}
