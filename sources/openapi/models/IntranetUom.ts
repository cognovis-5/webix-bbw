/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export enum IntranetUom {
    /**
     * \r\n
     */
    HOUR = 320,
    DAY = 321,
    UNIT = 322,
    WEEK = 328,
    MONTH = 329,
    MINPRICE = 331,
}
