/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { INamedId } from './INamedId';

export type IComponentPlugin = {
    /**
     * Plugin we display - Permission is on the plugin_id and the plugin_name can be used to match the portlet with the frontend class
     */
    plugin?: INamedId;
    /**
     * In which order should this plugin be displayed
     */
    sort_order?: number;
    /**
     * Where should we display the portlet - default options are left, right, top, buttom - but anything is possible if you know what to do with it.
     */
    location?: string;
    /**
     * Internationalized title to display for the portlet
     */
    title?: string;
    /**
     * Internationalization key for the title if it can be found / deducted from title_tcl
     */
    title_i18n_key?: string;
};

