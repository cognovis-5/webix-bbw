/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { INamedId } from './INamedId';

export type ICognovisObject = {
    /**
     * Identifier for the object
     */
    id?: number;
    /**
     * Name of the object
     */
    name?: string;
    /**
     * ACS Object type for the object
     */
    object_type?: string;
    /**
     * Category Type of the object
     */
    type?: INamedId;
    /**
     * Category Status of the object
     */
    status?: INamedId;
    /**
     * Class definition for the icon based of the (sub-)type of the object - taken from the aux_html2 of the type category_id
     */
    type_icon?: string;
    /**
     * Color code based of the status of the object - taken from the aux_html2 of the status category_id
     */
    status_color?: string;
};

