/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { IntegerInt32 } from './IntegerInt32';
import type { IntranetBizObjectRole } from './IntranetBizObjectRole';
import type { UserId } from './UserId';

export type IBizObjectMemberBody = {
    /**
     * Object that the user is a member of
     */
    object_id: IntegerInt32;
    /**
     * User who is a member of the object
     */
    member_id: UserId;
    /**
     * Role the user has in relation to the object
     */
    role_id?: IntranetBizObjectRole;
    /**
     * How much of their time are they in this role
     */
    percentage?: number;
};

