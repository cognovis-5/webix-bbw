/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type IMaterialObject = {
    /**
     * material_id of the Material
     */
    id?: number;
    /**
     * Material name
     */
    name?: string;
};

