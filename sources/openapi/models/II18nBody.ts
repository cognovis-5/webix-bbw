/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type II18nBody = {
    /**
     * Message Key we want to create
     */
    message_key: string;
    /**
     * Package in which we want to create the key
     */
    package_key: string;
    /**
     * Message we want to Store
     */
    message?: string;
    /**
     * Locale for which we want to register the message
     */
    locale?: string;
};

