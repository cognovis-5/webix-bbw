/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { ICognovisObject } from './ICognovisObject';
import type { IUser } from './IUser';

export type INote = {
    /**
     * Information about the person who added the note
     */
    creator?: IUser;
    /**
     * Note object information
     */
    note?: ICognovisObject;
    /**
     * HTML of the note
     */
    note_content?: string;
    /**
     * Time when the note was created
     */
    creation_date?: string;
};

