/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type IPostRegisterParty = {
    /**
     * Returns true if we could successfully add the party (or it already existed)
     */
    success?: boolean;
};

