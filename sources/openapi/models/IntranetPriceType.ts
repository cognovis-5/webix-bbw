/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export enum IntranetPriceType {
    DEFAULT = 3910,
    TIMESHEET = 3911,
    TRANSLATION = 3912,
}
