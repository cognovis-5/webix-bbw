/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { IntranetGanttTaskStatus } from './IntranetGanttTaskStatus';

export type IntranetGanttTaskStatusId = IntranetGanttTaskStatus;
