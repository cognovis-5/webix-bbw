/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export enum IntranetCompanyType {
    /**
     * \r\n      \r\n
     */
    UNKNOWN = 51,
    /**
     * \r\n
     */
    OTHER = 52,
    /**
     * \r\n
     */
    INTERNAL = 53,
    /**
     * \r\n      \r\n
     */
    MLV_TRANSLATION_AGENCY_COMPANY = 54,
    /**
     * \r\n      \r\n
     */
    SOFTWARE_COMPANY = 55,
    /**
     * Final Company
     */
    PROVIDER = 56,
    CUSTOMER = 57,
    FREELANCE_PROVIDER = 58,
    OFFICE_EQUIPMENT_PROVIDER = 59,
    LAW_COMPANY = 10244,
    IT_CONSULTANCY = 10245,
    CUSTORINTL = 10246,
    FINAL_COMPANY = 10247,
}
