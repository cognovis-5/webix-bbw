/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { IntranetBizObjectRole } from './IntranetBizObjectRole';

export type IntranetBizObjectRoleId = IntranetBizObjectRole;
