/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { IntranetPaymentTerm } from './IntranetPaymentTerm';

export type IntranetPaymentTermId = IntranetPaymentTerm;
