/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { IntranetInvoicePaymentMethod } from './IntranetInvoicePaymentMethod';

export type IntranetInvoicePaymentMethodId = IntranetInvoicePaymentMethod;
