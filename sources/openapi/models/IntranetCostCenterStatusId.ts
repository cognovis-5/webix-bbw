/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { IntranetCostCenterStatus } from './IntranetCostCenterStatus';

export type IntranetCostCenterStatusId = IntranetCostCenterStatus;
