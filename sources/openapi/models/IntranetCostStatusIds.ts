/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { IntranetCostStatus } from './IntranetCostStatus';

export type IntranetCostStatusIds = Array<IntranetCostStatus>;
