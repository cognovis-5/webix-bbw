/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export enum IntranetVatType {
    DOMESTIC_0 = 42000,
    DOMESTIC_7 = 42010,
    DOMESTIC_16 = 42020,
    EUROPE_0 = 42030,
    EUROPE_16 = 42040,
    INTERNAT_0 = 42050,
}
