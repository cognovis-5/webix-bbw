/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export enum IntranetGanttTaskStatus {
    CLOSED = 9601,
    ACTIVE = 9603,
}
