/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { ICategoryObject } from './ICategoryObject';
import type { INamedId } from './INamedId';

export type IInvoiceItem = {
    /**
     * Line item of the invoice
     */
    item?: INamedId;
    /**
     * Invoice in which we find the invoice item
     */
    invoice?: INamedId;
    /**
     * of units of the line item
     */
    item_units?: number;
    /**
     * unit of measure id of the line item
     */
    item_uom?: ICategoryObject;
    /**
     * price per unit of the line item
     */
    price_per_unit?: number;
    /**
     * currency ISO of the line item (e.g. EUR or USD)
     */
    currency?: string;
    /**
     * currency symbol to display in the line item
     */
    symbol?: string;
    /**
     * sequence order of the line item
     */
    sort_order?: number;
    /**
     * material of the line item
     */
    item_material?: INamedId;
    /**
     * Source Language of the Material
     */
    source_language?: ICategoryObject;
    /**
     * Target Language of the Material
     */
    target_language?: ICategoryObject;
    /**
     * Type of task associated with the Material
     */
    task_type?: ICategoryObject;
    /**
     * Subject Area of the associated material for the line item's material
     */
    subject_area?: ICategoryObject;
    /**
     * Description for the line item
     */
    description?: string;
    /**
     * context in which the line item was created
     */
    context?: INamedId;
};

