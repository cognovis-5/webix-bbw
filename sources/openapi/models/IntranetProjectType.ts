/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export enum IntranetProjectType {
    /**
     * Unknown project type
     */
    UNKNOWN = 85,
    /**
     * The project does not fit in any category
     */
    OTHER = 86,
    /**
     * Strategic consulting
     */
    STRATEGIC_CONSULTING = 97,
    /**
     * Ongoing software maintenance
     */
    SOFTWARE_MAINTENANCE = 98,
    /**
     * Software development
     */
    SOFTWARE_DEVELOPMENT = 99,
    /**
     * Helpdesk Ticket (should not be visible)
     */
    TICKET = 101,
    /**
     * Generic consulting project or any other project based on a Gantt schedule and Gantt tasks
     */
    GANTT_PROJECT = 2501,
    /**
     * Milestones appear in the Milestone dashboard
     */
    MILESTONE = 2504,
    /**
     * A group of projects with common resources or a common budget
     */
    PROGRAM = 2510,
    /**
     * Project using an agile methodology
     */
    EVENT = 2520,
    /**
     * Provides a task board with SCRUM states and a burn-down chart.
     */
    AGILE_PROJECT = 88000,
    /**
     * Provides a Kanban board and a burn-down diagram
     */
    SCRUM_SPRINT = 88010,
    KANBAN = 88020,
}
