/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { Id } from './Id';
import type { StringDate } from './StringDate';

export type ITimesheetEntryBody = {
    /**
     * Who has logged the time on this entry
     */
    user_id?: Id;
    /**
     * For which day was the entry
     */
    day?: StringDate;
    /**
     * What was done during that time - Shown to the customer
     */
    note?: string;
    /**
     * What was done during that time - Internal comment on the time logging, not shown to the customer
     */
    internal_note?: string;
    /**
     * how many hours were logged on that entry
     */
    hours: number;
};

