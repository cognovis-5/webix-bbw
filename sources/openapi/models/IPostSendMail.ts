/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type IPostSendMail = {
    /**
     * Id of the E-Mail send
     */
    message_id?: string;
};

