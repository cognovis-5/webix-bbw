/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { ICategoryObject } from './ICategoryObject';
import type { INamedId } from './INamedId';
import type { IUser } from './IUser';

export type IUserAbsence = {
    /**
     * User absence
     */
    absence?: INamedId;
    /**
     * Information about the person who was / will be absent
     */
    owner?: IUser;
    /**
     * Group we for which the absence was created
     */
    group?: INamedId;
    /**
     * When is the absence starting
     */
    start_date?: string;
    /**
     * when is absence ending
     */
    end_date?: string;
    /**
     * what kind of absence do we have
     */
    absence_type?: ICategoryObject;
    /**
     * what is current status of absence
     */
    absence_status?: ICategoryObject;
    /**
     * description (reason) for the absence
     */
    description?: string;
    /**
     * automaticaly calculated duration
     */
    duration?: number;
};

