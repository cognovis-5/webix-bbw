/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { ICognovisObject } from './ICognovisObject';
import type { IReportColumn } from './IReportColumn';
import type { IReportRow } from './IReportRow';

export type IReport = {
    /**
     * Report object information
     */
    report?: ICognovisObject;
    /**
     * Description of the report
     */
    report_description?: string;
    /**
     * Definition for the columns of the report
     */
    report_columns?: Array<IReportColumn>;
    /**
     * Rows with the data of the table
     */
    report_rows?: Array<IReportRow>;
};

