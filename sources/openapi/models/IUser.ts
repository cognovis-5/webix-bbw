/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { ICognovisObject } from './ICognovisObject';

export type IUser = {
    /**
     * Username and ID of the user
     */
    user?: ICognovisObject;
    /**
     * Screenname used as an Alias
     */
    screen_name?: string;
    /**
     * Locale to use in communication
     */
    locale?: string;
    /**
     * How many seconds since this user was last seen on the site (not with this object)
     */
    seconds_since_last_request?: number;
    /**
     * URL to the portrait of the user
     */
    portrait_url?: string;
    /**
     * Main company for the user
     */
    company?: ICognovisObject;
};

