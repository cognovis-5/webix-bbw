/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { ICategoryObject } from './ICategoryObject';
import type { INamedId } from './INamedId';
import type { IUser } from './IUser';

export type ITimesheetTask = {
    /**
     * Timesheet task
     */
    task?: INamedId;
    /**
     * Information about the person who added the note
     */
    task_assignee?: IUser;
    /**
     * Status_id of the task while it is being worked on
     */
    task_status?: ICategoryObject;
    /**
     * Status_id of the task how it is communicated to the outside
     */
    project_status?: ICategoryObject;
    /**
     * Task_id of the task
     */
    task_type?: ICategoryObject;
    /**
     * How much of the task is already finished. Does not have to relate to hours
     */
    percent_completed?: number;
    /**
     * How many units are planned for this task
     */
    planned_units?: number;
    /**
     * How many units can you charge the customer for
     */
    billable_units?: number;
    /**
     * How many hours have been logged on the task
     */
    logged_hours?: number;
    /**
     * When is the task starting
     */
    start_date?: string;
    /**
     * Date for the deadline
     */
    end_date?: string;
    /**
     * Project in which the task is located
     */
    project?: INamedId;
};

