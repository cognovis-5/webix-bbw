/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { Id } from './Id';
import type { IntegerInt32 } from './IntegerInt32';
import type { IntranetCompanyStatus } from './IntranetCompanyStatus';
import type { IntranetCompanyType } from './IntranetCompanyType';
import type { IntranetCostTemplate } from './IntranetCostTemplate';
import type { IntranetPaymentTerm } from './IntranetPaymentTerm';
import type { IntranetVatType } from './IntranetVatType';

export type ICompanyBody = {
    /**
     * Name of the company
     */
    company_name: string;
    /**
     * Internal name for the company. Useful if you have multiple companies with the same name. Needs to be unique!
     */
    comapny_path?: string;
    /**
     * Type of company (Intranet Company Type)
     */
    company_type_id?: IntranetCompanyType;
    /**
     * Status of company (Intranet Company Status)
     */
    company_status_id?: IntranetCompanyStatus;
    /**
     * VAT Classification of the company
     */
    vat_type_id?: IntranetVatType;
    /**
     * company vat number
     */
    vat_number?: string;
    /**
     * Primary contact (ID) for the company
     */
    primary_contact_id?: IntegerInt32;
    /**
     * Accounting contact (ID) for the company
     */
    accounting_contact_id?: IntegerInt32;
    /**
     * Country code of company main_office_id
     */
    address_country_code?: string;
    /**
     * Street (address_line_1) of company main office
     */
    address_line1?: string;
    /**
     * Second address line (e.g. PO box or appartment)
     */
    address_line2?: string;
    /**
     * city of company main office
     */
    address_city?: string;
    /**
     * zip code / postal code of company main office
     */
    address_postal_code?: string;
    /**
     * phone number of the main office
     */
    phone?: string;
    /**
     * Website for the company
     */
    url?: string;
    /**
     * Where did this company come from
     */
    referral_source_id?: Id;
    /**
     * Terms for the payment (aka how fast has the payment to be made)
     */
    payment_term_id?: IntranetPaymentTerm;
    /**
     * Template we use for new invoices for this customer
     */
    default_invoice_template_id?: IntranetCostTemplate;
    /**
     * Template we use for new quotes for this customer
     */
    default_quote_template_id?: IntranetCostTemplate;
    /**
     * Template we use for new provider bills
     */
    default_bill_template_id?: IntranetCostTemplate;
    /**
     * Template we use for new purchase orders for this provider
     */
    default_po_template_id?: IntranetCostTemplate;
};

