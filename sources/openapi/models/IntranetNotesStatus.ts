/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export enum IntranetNotesStatus {
    ACTIVE = 11400,
    DELETED = 11402,
}
