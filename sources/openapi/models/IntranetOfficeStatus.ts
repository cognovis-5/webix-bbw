/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export enum IntranetOfficeStatus {
    ACTIVE = 160,
    INACTIVE = 161,
}
