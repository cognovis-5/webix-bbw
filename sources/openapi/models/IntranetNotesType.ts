/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export enum IntranetNotesType {
    ADDRESS = 11500,
    EMAIL = 11502,
    HTTP = 11504,
    FTP = 11506,
    PHONE = 11508,
    FAX = 11510,
    MOBILE = 11512,
    OTHER = 11514,
    PARTICIPANT_NOTE = 11520,
}
