/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { IntranetCostTemplate } from './IntranetCostTemplate';

export type IntranetCostTemplateId = IntranetCostTemplate;
