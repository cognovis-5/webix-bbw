/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { IntranetAbsenceType } from './IntranetAbsenceType';

export type IntranetAbsenceTypeId = IntranetAbsenceType;
