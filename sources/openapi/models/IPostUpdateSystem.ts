/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type IPostUpdateSystem = {
    /**
     * Key of the package which can be updated
     */
    package_key?: string;
    /**
     * Name of the package (more extensive description)
     */
    package_name?: string;
    /**
     * Version number of the new package
     */
    new_version?: string;
    /**
     * Version number of the currently installed package
     */
    installed_version?: string;
    /**
     * Message of the error if we were not successful in upgrading the package
     */
    err_msg?: string;
};

