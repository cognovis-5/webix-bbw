/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { Id } from './Id';
import type { ImCompanyId } from './ImCompanyId';
import type { ImMaterialId } from './ImMaterialId';
import type { IntranetPriceComplexity } from './IntranetPriceComplexity';
import type { IntranetPriceStatus } from './IntranetPriceStatus';
import type { IntranetPriceType } from './IntranetPriceType';
import type { IntranetProjectType } from './IntranetProjectType';
import type { IntranetUom } from './IntranetUom';
import type { StringDate } from './StringDate';

export type IPriceBody = {
    /**
     * Type of price
     */
    price_type_id?: IntranetPriceType;
    /**
     * Status of the price
     */
    price_status_id?: IntranetPriceStatus;
    /**
     * Material for which this price is valid. STRONGLY ENCOURAGE TO USE (will overwrite other material_ parameters)
     */
    material_id?: ImMaterialId;
    /**
     * Company for which this price is valid
     */
    company_id: ImCompanyId;
    /**
     * Complexity of the work
     */
    complexity_type_id?: IntranetPriceComplexity;
    /**
     * Since when is this price valid
     */
    valid_from?: StringDate;
    /**
     * How long is it valid (defaults to nothing = unlimited)
     */
    valid_through?: StringDate;
    /**
     * ISO for the currency
     */
    currency?: string;
    /**
     * Actual price per Unit
     */
    price: number;
    /**
     * Minimum price charged in case units * price is lower
     */
    min_price?: number;
    /**
     * Note associated with this price
     */
    note?: string;
    /**
     * Unit of Measure for the material
     */
    material_uom_id?: IntranetUom;
    /**
     * which is the source language for this material
     */
    material_source_language_id?: Id;
    /**
     * target language (if any)
     */
    material_target_language_id?: Id;
    /**
     * Subject area for the translation
     */
    material_subject_area_id?: Id;
    /**
     * what type of action do we make for this material
     */
    material_task_type_id?: IntranetProjectType;
    /**
     * File type we work on
     */
    material_file_type_id?: Id;
};

