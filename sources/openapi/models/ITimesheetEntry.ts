/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { INamedId } from './INamedId';

export type ITimesheetEntry = {
    /**
     * Identifier for the timesheet entry
     */
    hour_id?: number;
    /**
     * Task ID and name
     */
    task?: INamedId;
    /**
     * Project in which the task is located
     */
    project?: INamedId;
    /**
     * user Who has logged the time on this entry
     */
    user?: INamedId;
    /**
     * For which day was the entry
     */
    day?: string;
    /**
     * When was the entry added
     */
    creation_date?: string;
    /**
     * What was done during that time - as shown to the customer
     */
    note?: string;
    /**
     * What was done during that time - Internal comment on the time logging, not shown to the customer
     */
    internal_note?: string;
    /**
     * how many hours were logged on that entry
     */
    hours?: number;
};

