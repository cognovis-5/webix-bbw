/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { IntranetMaterialStatus } from './IntranetMaterialStatus';

export type IntranetMaterialStatusId = IntranetMaterialStatus;
