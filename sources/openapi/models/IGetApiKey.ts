/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type IGetApiKey = {
    /**
     * API Key for the currently logged in user
     */
    api_key?: string;
};

