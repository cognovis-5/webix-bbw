/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export enum IntranetInvoicePaymentMethod {
    /**
     * Not defined yet
     */
    UNDEFINED = 800,
    /**
     * Cash or cash equivalent
     */
    CASH = 802,
    BANK_TRANSFER = 808,
    PAYPAL = 810,
}
