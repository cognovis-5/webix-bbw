/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type ICountry = {
    /**
     * Iso Code of the country
     */
    iso?: string;
    /**
     * Country name
     */
    country_name?: string;
};

