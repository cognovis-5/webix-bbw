/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { Boolean0 } from './Boolean0';
import type { DanceRole } from './DanceRole';
import type { EventParticipantLevel } from './EventParticipantLevel';
import type { ImProjectId } from './ImProjectId';
import type { IntegerInt32 } from './IntegerInt32';

export type IBbwRegistrationBody = {
    /**
     * project_id is used as event id (single event = single project_id)
     */
    project_id?: ImProjectId;
    /**
     * first names of event participant
     */
    first_names?: string;
    /**
     * last name of event participant
     */
    last_name?: string;
    /**
     * email of event participant
     */
    email?: string;
    /**
     * participant company address line 1
     */
    ha_line1: string;
    /**
     * participant city name
     */
    ha_city: string;
    /**
     * participant company state
     */
    ha_state?: string;
    /**
     * string participant company country
     */
    ha_country_code: string;
    /**
     * particippant postal code (or zip)
     */
    ha_postal_code: string;
    /**
     * Material of the course the user wants to attend
     */
    course_material_id: IntegerInt32;
    /**
     * Dance type - Lead or Follow
     */
    event_participant_type_id: DanceRole;
    /**
     * Level of the participant
     */
    event_participant_level_id?: EventParticipantLevel;
    /**
     * e.g. name of potential dance partners
     */
    event_partners_text?: string;
    /**
     * is participant interested in party pass?
     */
    party_pass_interested_p?: Boolean0;
    /**
     * Comments added by the participant
     */
    comments?: string;
};

