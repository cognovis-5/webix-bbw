/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { EventParticipantLevel } from './EventParticipantLevel';

export type EventParticipantLevelId = EventParticipantLevel;
