/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { IIdValue } from './IIdValue';

export type IdValue = Array<IIdValue>;
