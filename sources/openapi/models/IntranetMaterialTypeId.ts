/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { IntranetMaterialType } from './IntranetMaterialType';

export type IntranetMaterialTypeId = IntranetMaterialType;
