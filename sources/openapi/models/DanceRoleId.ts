/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { DanceRole } from './DanceRole';

export type DanceRoleId = DanceRole;
