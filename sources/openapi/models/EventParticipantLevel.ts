/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export enum EventParticipantLevel {
    BABY = 82550,
    TEEN = 82551,
    ADULT = 82552,
    OLDIE = 10000022,
}
