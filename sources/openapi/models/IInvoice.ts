/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { ICategoryObject } from './ICategoryObject';
import type { ICognovisObject } from './ICognovisObject';
import type { INamedId } from './INamedId';

export type IInvoice = {
    /**
     * Invoice item
     */
    invoice?: INamedId;
    /**
     * Project in which the invoice resides
     */
    project?: INamedId;
    /**
     * Company which is the recipient / provider for this invoice (aka the 'other' party)
     */
    company?: INamedId;
    /**
     * Contact for the invoice
     */
    company_contact?: INamedId;
    /**
     * Company who is the providing contact for the invoice. Should be an internal company.
     */
    provider?: INamedId;
    /**
     * First name of the Contact
     */
    first_names?: string;
    /**
     * Last name of the Contact
     */
    last_name?: string;
    /**
     * E-mail address of the contact
     */
    email?: string;
    /**
     * Phone number under which the user can be reached. cell_phone > work_phone > private_phone
     */
    phone?: string;
    /**
     * Office which is to recieve the invoice
     */
    invoice_office?: INamedId;
    /**
     * Usually the street address or c/o
     */
    address_line1?: string;
    /**
     * Additional address line if needed
     */
    address_line2?: string;
    /**
     * City in which the office resides
     */
    address_city?: string;
    /**
     * State of the office
     */
    address_state?: string;
    /**
     * Postal code for the invoice
     */
    address_postal_code?: string;
    /**
     * Actual country name (internationalized)
     */
    address_country?: string;
    /**
     * Who created the invoice
     */
    creation_user?: INamedId;
    /**
     * When was the invoice created
     */
    creation_date?: string;
    /**
     * Name of the cost center for the invoice
     */
    cost_center?: INamedId;
    /**
     * Status of the invoice
     */
    cost_status?: ICategoryObject;
    /**
     * Type of the invoice
     */
    cost_type?: ICategoryObject;
    /**
     * Template used for printing the invoice
     */
    template?: INamedId;
    /**
     * Method of payment
     */
    payment_method?: ICategoryObject;
    /**
     * Terms for the payment (aka how fast has the payment to be made)
     */
    payment_term?: ICategoryObject;
    /**
     * Name of the currency
     */
    currency_name?: string;
    /**
     * Symbol for the currency to be used
     */
    currency_symbol?: string;
    /**
     * Type of VAT, used for calculation
     */
    vat_type?: ICategoryObject;
    /**
     * When were the services provided (especially if the invoice is issued later)
     */
    delivery_date?: string;
    /**
     * The actual invoice date (to be printed on the invoice pdf)
     */
    effective_date?: string;
    /**
     * linked invoice name and id. Invoice in case of quote, Provider Bill in case of PO
     */
    linked_invoice?: INamedId;
    /**
     * Linked objects for this invoice
     */
    linked_objects?: Array<ICognovisObject>;
    /**
     * Do we have linked objects
     */
    linked_objects_p?: boolean;
    /**
     * Amount for the invoice before VAT
     */
    amount?: number;
    /**
     * value (percentage) of VAT
     */
    vat?: number;
    /**
     * VAT for the invoice
     */
    vat_amount?: number;
    /**
     * Grand total for the invoice
     */
    grand_total?: number;
};

