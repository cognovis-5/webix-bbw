/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { ICategoryObject } from './ICategoryObject';
import type { INamedId } from './INamedId';
import type { IUser } from './IUser';

export type IBbwRegistration = {
    /**
     * Participation_id for this event
     */
    participant_id?: number;
    /**
     * User who is participating
     */
    participant?: IUser;
    /**
     * first names of event participant
     */
    first_names?: string;
    /**
     * last name of event participant
     */
    last_name?: string;
    /**
     * email of event participant
     */
    email?: string;
    /**
     * project is used as event id (single event = single project_id)
     */
    project?: INamedId;
    /**
     * participant company address line 1
     */
    ha_line1?: string;
    /**
     * participant city name
     */
    ha_city?: string;
    /**
     * participant company state
     */
    ha_state?: string;
    /**
     * string participant company country
     */
    ha_country_code?: string;
    /**
     * particippant postal code (or zip)
     */
    ha_postal_code?: string;
    /**
     * Material of the course the user wants to attend
     */
    course_material_id?: number;
    /**
     * Dance type - Lead or Follow
     */
    event_participant_type?: ICategoryObject;
    /**
     * Level of the participant
     */
    event_participant_level?: ICategoryObject;
    /**
     * e.g. name of potential dance partners
     */
    event_partners_text?: string;
    /**
     * Comments added by the participant
     */
    comments?: string;
    /**
     * is participant interested in party pass?
     */
    party_pass_interested_p?: boolean;
};

