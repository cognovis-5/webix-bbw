/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { IntranetCompanyStatus } from './IntranetCompanyStatus';

export type IntranetCompanyStatusIds = Array<IntranetCompanyStatus>;
