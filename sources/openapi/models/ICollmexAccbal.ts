/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { INamedId } from './INamedId';

export type ICollmexAccbal = {
    /**
     * Date of the balance
     */
    bal_date?: string;
    /**
     * Account of the amount in the bal_date
     */
    account?: INamedId;
    /**
     * How big was the balance for the account on the given date
     */
    amount?: number;
};

