/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { IntranetGanttTaskType } from './IntranetGanttTaskType';

export type IntranetGanttTaskTypeId = IntranetGanttTaskType;
