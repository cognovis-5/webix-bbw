/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { IntranetPriceType } from './IntranetPriceType';

export type IntranetPriceTypeId = IntranetPriceType;
