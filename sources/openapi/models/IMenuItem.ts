/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type IMenuItem = {
    /**
     * ID of the menu
     */
    menu_id?: number;
    /**
     * of parent
     */
    parent_menu_id?: number;
    /**
     * Translated name for the menu
     */
    name?: string;
    /**
     * Position of the menu item in the menu list
     */
    sort_order?: number;
    /**
     * Routing information
     */
    url?: string;
    /**
     * Icon to use, can be 'fa fa-stop' or 'mdimdi-stop'
     */
    icon?: string;
    /**
     * Label for the menu, used in lookups of parent_menu_id. Like a menu_nr. Not making any sense, should be swapped with name, but thats how project-open thinks
     */
    label?: string;
};

