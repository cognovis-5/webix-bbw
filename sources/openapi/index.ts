/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
export { ApiError } from './core/ApiError';
export { CancelablePromise, CancelError } from './core/CancelablePromise';
export { OpenAPI } from './core/OpenAPI';
export type { OpenAPIConfig } from './core/OpenAPI';

export type { Boolean0 } from './models/Boolean0';
export type { Boolean1 } from './models/Boolean1';
export type { BooleanT } from './models/BooleanT';
export { DanceRole } from './models/DanceRole';
export type { DanceRoleId } from './models/DanceRoleId';
export { EventParticipantLevel } from './models/EventParticipantLevel';
export type { EventParticipantLevelId } from './models/EventParticipantLevelId';
export type { IBbwRegistration } from './models/IBbwRegistration';
export type { IBbwRegistrationBody } from './models/IBbwRegistrationBody';
export type { IBizObjectMember } from './models/IBizObjectMember';
export type { IBizObjectMemberBody } from './models/IBizObjectMemberBody';
export type { ICategory } from './models/ICategory';
export type { ICategoryArray } from './models/ICategoryArray';
export type { ICategoryObject } from './models/ICategoryObject';
export type { ICognovisObject } from './models/ICognovisObject';
export type { ICollmexAccbal } from './models/ICollmexAccbal';
export type { ICollmexAccdoc } from './models/ICollmexAccdoc';
export type { ICompany } from './models/ICompany';
export type { ICompanyBody } from './models/ICompanyBody';
export type { ICompanyContact } from './models/ICompanyContact';
export type { ICompanyContactBody } from './models/ICompanyContactBody';
export type { IComponentPlugin } from './models/IComponentPlugin';
export type { ICostCenter } from './models/ICostCenter';
export type { ICountry } from './models/ICountry';
export type { ICrFile } from './models/ICrFile';
export type { ICrFolder } from './models/ICrFolder';
export type { Id } from './models/Id';
export type { IDayOff } from './models/IDayOff';
export type { Ids } from './models/Ids';
export type { IdValue } from './models/IdValue';
export type { IDynamicAttribute } from './models/IDynamicAttribute';
export type { IDynamicObjectBody } from './models/IDynamicObjectBody';
export type { IDynamicSubtype } from './models/IDynamicSubtype';
export type { IError } from './models/IError';
export type { IGetApiKey } from './models/IGetApiKey';
export type { IGetPlugin } from './models/IGetPlugin';
export type { IGetServerUp } from './models/IGetServerUp';
export type { IGetSwitchUser } from './models/IGetSwitchUser';
export type { IGetUpdateSystem } from './models/IGetUpdateSystem';
export type { IGroup } from './models/IGroup';
export type { II18nBody } from './models/II18nBody';
export type { IIdValue } from './models/IIdValue';
export type { IInternalCompany } from './models/IInternalCompany';
export type { IInvoice } from './models/IInvoice';
export type { IInvoiceBody } from './models/IInvoiceBody';
export type { IInvoiceBodyPut } from './models/IInvoiceBodyPut';
export type { IInvoiceItem } from './models/IInvoiceItem';
export type { IInvoiceItemBody } from './models/IInvoiceItemBody';
export type { IInvoiceItemBodyPut } from './models/IInvoiceItemBodyPut';
export type { IInvoiceTimesheetTasksPut } from './models/IInvoiceTimesheetTasksPut';
export type { IMail } from './models/IMail';
export type { IMailBody } from './models/IMailBody';
export type { IMailPerson } from './models/IMailPerson';
export type { IMaterial } from './models/IMaterial';
export type { IMaterialArray } from './models/IMaterialArray';
export type { IMaterialBody } from './models/IMaterialBody';
export type { IMaterialObject } from './models/IMaterialObject';
export type { ImCompanyId } from './models/ImCompanyId';
export type { ImCostCenterId } from './models/ImCostCenterId';
export type { IMenuItem } from './models/IMenuItem';
export type { ImInvoiceId } from './models/ImInvoiceId';
export type { ImInvoiceIds } from './models/ImInvoiceIds';
export type { ImMaterialId } from './models/ImMaterialId';
export type { ImNoteId } from './models/ImNoteId';
export type { ImPriceId } from './models/ImPriceId';
export type { ImProfileId } from './models/ImProfileId';
export type { ImProjectId } from './models/ImProjectId';
export type { ImReportId } from './models/ImReportId';
export type { ImTimesheetTaskId } from './models/ImTimesheetTaskId';
export type { ImUserAbsenceId } from './models/ImUserAbsenceId';
export type { INamedId } from './models/INamedId';
export type { INamedObject } from './models/INamedObject';
export type { INote } from './models/INote';
export type { INoteBody } from './models/INoteBody';
export type { IntegerInt32 } from './models/IntegerInt32';
export type { IntegerInt320 } from './models/IntegerInt320';
export { IntranetAbsenceStatus } from './models/IntranetAbsenceStatus';
export type { IntranetAbsenceStatusId } from './models/IntranetAbsenceStatusId';
export { IntranetAbsenceType } from './models/IntranetAbsenceType';
export type { IntranetAbsenceTypeId } from './models/IntranetAbsenceTypeId';
export { IntranetBizObjectRole } from './models/IntranetBizObjectRole';
export type { IntranetBizObjectRoleId } from './models/IntranetBizObjectRoleId';
export type { IntranetBizObjectRoleIds } from './models/IntranetBizObjectRoleIds';
export { IntranetCompanyStatus } from './models/IntranetCompanyStatus';
export type { IntranetCompanyStatusId } from './models/IntranetCompanyStatusId';
export type { IntranetCompanyStatusIds } from './models/IntranetCompanyStatusIds';
export { IntranetCompanyType } from './models/IntranetCompanyType';
export type { IntranetCompanyTypeId } from './models/IntranetCompanyTypeId';
export type { IntranetCompanyTypeIds } from './models/IntranetCompanyTypeIds';
export { IntranetCostCenterStatus } from './models/IntranetCostCenterStatus';
export type { IntranetCostCenterStatusId } from './models/IntranetCostCenterStatusId';
export { IntranetCostCenterType } from './models/IntranetCostCenterType';
export type { IntranetCostCenterTypeId } from './models/IntranetCostCenterTypeId';
export { IntranetCostStatus } from './models/IntranetCostStatus';
export type { IntranetCostStatusId } from './models/IntranetCostStatusId';
export type { IntranetCostStatusIds } from './models/IntranetCostStatusIds';
export { IntranetCostTemplate } from './models/IntranetCostTemplate';
export type { IntranetCostTemplateId } from './models/IntranetCostTemplateId';
export { IntranetCostType } from './models/IntranetCostType';
export type { IntranetCostTypeId } from './models/IntranetCostTypeId';
export type { IntranetCostTypeIds } from './models/IntranetCostTypeIds';
export { IntranetGanttTaskStatus } from './models/IntranetGanttTaskStatus';
export type { IntranetGanttTaskStatusId } from './models/IntranetGanttTaskStatusId';
export { IntranetGanttTaskType } from './models/IntranetGanttTaskType';
export type { IntranetGanttTaskTypeId } from './models/IntranetGanttTaskTypeId';
export { IntranetInvoicePaymentMethod } from './models/IntranetInvoicePaymentMethod';
export type { IntranetInvoicePaymentMethodId } from './models/IntranetInvoicePaymentMethodId';
export { IntranetMaterialStatus } from './models/IntranetMaterialStatus';
export type { IntranetMaterialStatusId } from './models/IntranetMaterialStatusId';
export { IntranetMaterialType } from './models/IntranetMaterialType';
export type { IntranetMaterialTypeId } from './models/IntranetMaterialTypeId';
export { IntranetNotesStatus } from './models/IntranetNotesStatus';
export type { IntranetNotesStatusId } from './models/IntranetNotesStatusId';
export { IntranetNotesType } from './models/IntranetNotesType';
export type { IntranetNotesTypeId } from './models/IntranetNotesTypeId';
export { IntranetOfficeStatus } from './models/IntranetOfficeStatus';
export type { IntranetOfficeStatusId160 } from './models/IntranetOfficeStatusId160';
export { IntranetOfficeType } from './models/IntranetOfficeType';
export type { IntranetOfficeTypeId170 } from './models/IntranetOfficeTypeId170';
export { IntranetPaymentTerm } from './models/IntranetPaymentTerm';
export type { IntranetPaymentTermId } from './models/IntranetPaymentTermId';
export { IntranetPriceComplexity } from './models/IntranetPriceComplexity';
export type { IntranetPriceComplexityId } from './models/IntranetPriceComplexityId';
export { IntranetPriceStatus } from './models/IntranetPriceStatus';
export type { IntranetPriceStatusId } from './models/IntranetPriceStatusId';
export { IntranetPriceType } from './models/IntranetPriceType';
export type { IntranetPriceTypeId } from './models/IntranetPriceTypeId';
export { IntranetProjectType } from './models/IntranetProjectType';
export type { IntranetProjectTypeId } from './models/IntranetProjectTypeId';
export { IntranetSalutation } from './models/IntranetSalutation';
export type { IntranetSalutationId } from './models/IntranetSalutationId';
export { IntranetUom } from './models/IntranetUom';
export type { IntranetUomId } from './models/IntranetUomId';
export { IntranetVatType } from './models/IntranetVatType';
export type { IntranetVatTypeId } from './models/IntranetVatTypeId';
export type { IObjectType } from './models/IObjectType';
export type { IOffice } from './models/IOffice';
export type { IParameter } from './models/IParameter';
export type { IPostRegisterParty } from './models/IPostRegisterParty';
export type { IPostSendMail } from './models/IPostSendMail';
export type { IPostUpdateSystem } from './models/IPostUpdateSystem';
export type { IPrice } from './models/IPrice';
export type { IPriceBody } from './models/IPriceBody';
export type { IPrivilege } from './models/IPrivilege';
export type { IRelationship } from './models/IRelationship';
export type { IRelationshipBody } from './models/IRelationshipBody';
export type { IReport } from './models/IReport';
export type { IReportColumn } from './models/IReportColumn';
export type { IReportRow } from './models/IReportRow';
export type { IServerError500 } from './models/IServerError500';
export type { ITimesheetEntry } from './models/ITimesheetEntry';
export type { ITimesheetEntryBody } from './models/ITimesheetEntryBody';
export type { ITimesheetTask } from './models/ITimesheetTask';
export type { ITimesheetTaskBody } from './models/ITimesheetTaskBody';
export type { ITranslation } from './models/ITranslation';
export type { IUser } from './models/IUser';
export type { IUserAbsence } from './models/IUserAbsence';
export type { IUserAbsenceBody } from './models/IUserAbsenceBody';
export type { IUserError404 } from './models/IUserError404';
export type { IUserToken } from './models/IUserToken';
export type { NumberFloat } from './models/NumberFloat';
export type { PackageParameterPackageKeys } from './models/PackageParameterPackageKeys';
export { Pagination } from './models/Pagination';
export type { PartyId } from './models/PartyId';
export type { PartyIds } from './models/PartyIds';
export type { PersonId } from './models/PersonId';
export type { PersonIds } from './models/PersonIds';
export type { String1 } from './models/String1';
export type { StringDate } from './models/StringDate';
export type { StringDateTime } from './models/StringDateTime';
export type { StringDefault } from './models/StringDefault';
export type { StringPdf } from './models/StringPdf';
export type { StringSentDateDesc } from './models/StringSentDateDesc';
export type { StringTypescript } from './models/StringTypescript';
export type { StringUpload } from './models/StringUpload';
export type { UserId } from './models/UserId';
export type { UserIds } from './models/UserIds';

export { CognovisRestService } from './services/CognovisRestService';
export { CognovisRestAbsencesService } from './services/CognovisRestAbsencesService';
export { CognovisRestCompanyService } from './services/CognovisRestCompanyService';
export { CognovisRestDynamicService } from './services/CognovisRestDynamicService';
export { CognovisRestFileService } from './services/CognovisRestFileService';
export { CognovisRestInvoiceService } from './services/CognovisRestInvoiceService';
export { CognovisRestMailService } from './services/CognovisRestMailService';
export { CognovisRestNotesService } from './services/CognovisRestNotesService';
export { CognovisRestObjectService } from './services/CognovisRestObjectService';
export { CognovisRestOpenapiService } from './services/CognovisRestOpenapiService';
export { CognovisRestPricingService } from './services/CognovisRestPricingService';
export { CognovisRestRelationshipService } from './services/CognovisRestRelationshipService';
export { CognovisRestReportService } from './services/CognovisRestReportService';
export { CognovisRestSystemService } from './services/CognovisRestSystemService';
export { CognovisRestTimesheetService } from './services/CognovisRestTimesheetService';
export { IntranetCollmexRestService } from './services/IntranetCollmexRestService';
export { IntranetCustBbwRestService } from './services/IntranetCustBbwRestService';
