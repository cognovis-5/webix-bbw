/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { IComponentPlugin } from '../models/IComponentPlugin';
import type { IDynamicAttribute } from '../models/IDynamicAttribute';
import type { IDynamicObjectBody } from '../models/IDynamicObjectBody';
import type { IDynamicSubtype } from '../models/IDynamicSubtype';
import type { IIdValue } from '../models/IIdValue';
import type { IntegerInt32 } from '../models/IntegerInt32';
import type { StringDefault } from '../models/StringDefault';

import type { CancelablePromise } from '../core/CancelablePromise';
import { OpenAPI } from '../core/OpenAPI';
import { request as __request } from '../core/request';

export class CognovisRestDynamicService {

    /**
     * Returns an array of portlets to display for an object at a given URL - they are component_plugins in the database, hence the<br>    component_plugin datatype
     * @returns IComponentPlugin Array of portlets to display.
     * @throws ApiError
     */
    public static getComponentPlugins({
        pageUrl,
    }: {
        /**
         * URL in which we want to display the portlets
         */
        pageUrl: string,
    }): CancelablePromise<Array<IComponentPlugin>> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/component_plugins',
            query: {
                'page_url': pageUrl,
            },
        });
    }

    /**
     * Returns the widget information for a given object_type.
     * @returns IDynamicAttribute Array of attributes to display / edit
     * @throws ApiError
     */
    public static getDynamicAttributes({
        objectType,
        objectSubtypeId,
        objectId,
        pageUrl,
        parameterObjectJson,
    }: {
        /**
         * Object Type we look for (e.g. persons)
         */
        objectType: string,
        /**
         * Category ID for the subtype we want to get information for
         */
        objectSubtypeId?: IntegerInt32,
        /**
         * Object we look at. Necessary for persons or anything with multiple subtypes. Only use if you can't pass a single subtype_id
         */
        objectId?: IntegerInt32,
        /**
         * What is the URL where we display the form. should be in the form of /#!/...... - determines sort_order and which attributes to actually display (irrespective of subtype configuration)
         */
        pageUrl?: StringDefault,
        /**
         * JSON object with additional key/values that might be needed by the widget to return widget_data
         */
        parameterObjectJson?: string,
    }): CancelablePromise<Array<IDynamicAttribute>> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/dynamic_attributes',
            query: {
                'object_type': objectType,
                'object_subtype_id': objectSubtypeId,
                'object_id': objectId,
                'page_url': pageUrl,
                'parameter_object_json': parameterObjectJson,
            },
        });
    }

    /**
     * Returns an array of key/value pairs for an object
     * @returns IIdValue Array of id+values for the object. ID is the attribute_name and value is the corresponding value.
     * @throws ApiError
     */
    public static getDynamicObject({
        objectId,
        pageUrl,
    }: {
        /**
         * Object we want to retrieve. Will check read permission
         */
        objectId: IntegerInt32,
        /**
         * PageURL to limit the values we want to return for the object
         */
        pageUrl?: StringDefault,
    }): CancelablePromise<Array<IIdValue>> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/dynamic_object',
            query: {
                'object_id': objectId,
                'page_url': pageUrl,
            },
        });
    }

    /**
     * Returns an array of key/value pairs for an object
     * @returns IIdValue Array of id+values for the object. ID is the attribute_name and value is the corresponding value.
     * @throws ApiError
     */
    public static putDynamicObject({
        requestBody,
        pageUrl,
    }: {
        /**
         * Object we want to update
         */
        requestBody: IDynamicObjectBody,
        /**
         * PageURL to limit the values we want to return for the object
         */
        pageUrl?: StringDefault,
    }): CancelablePromise<Array<IIdValue>> {
        return __request(OpenAPI, {
            method: 'PUT',
            url: '/dynamic_object',
            query: {
                'page_url': pageUrl,
            },
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * Return the dynamic subtypes for an attribute to know how it is configured
     * @returns IDynamicSubtype Array of subtypes and if we show the attribute for them
     * @throws ApiError
     */
    public static getDynamicSubtypes({
        attributeId,
    }: {
        /**
         * dynfield attribute we want to get subtype permission info
         */
        attributeId?: IntegerInt32,
    }): CancelablePromise<Array<IDynamicSubtype>> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/dynamic_subtypes',
            query: {
                'attribute_id': attributeId,
            },
        });
    }

}
