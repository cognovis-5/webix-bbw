/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { IBbwRegistration } from '../models/IBbwRegistration';
import type { IBbwRegistrationBody } from '../models/IBbwRegistrationBody';
import type { Id } from '../models/Id';
import type { ImProjectId } from '../models/ImProjectId';
import type { IUserToken } from '../models/IUserToken';

import type { CancelablePromise } from '../core/CancelablePromise';
import { OpenAPI } from '../core/OpenAPI';
import { request as __request } from '../core/request';

export class IntranetCustBbwRestService {

    /**
     * Returns the registration for a BBW event
     * @returns IBbwRegistration The info from the actual registration
     * @throws ApiError
     */
    public static getBbwRegistration({
        projectId,
        email,
        participantId,
    }: {
        /**
         * project_id is used as event id (single event = single project_id)
         */
        projectId?: ImProjectId,
        /**
         * email of event participant
         */
        email?: string,
        /**
         * Participation inforamtion
         */
        participantId?: Id,
    }): CancelablePromise<IBbwRegistration> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/bbw_registration',
            query: {
                'project_id': projectId,
                'email': email,
                'participant_id': participantId,
            },
        });
    }

    /**
     * Handler for new registrations
     * @returns IBbwRegistration The info from the actual registration
     * @throws ApiError
     */
    public static postBbwRegistration({
        requestBody,
    }: {
        /**
         * Registration info for the bbw
         */
        requestBody: IBbwRegistrationBody,
    }): CancelablePromise<IBbwRegistration> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/bbw_registration',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * Registers a user for login. If names are provided, create a person in addition<br>        Allows callbacks to act upon it.
     * @returns IUserToken Party with authorization information
     * @throws ApiError
     */
    public static postRegisterUser({
        email,
        firstNames,
        lastName,
    }: {
        /**
         * E-Mail we want to register
         */
        email: string,
        /**
         * First names of the registered user
         */
        firstNames?: string,
        /**
         * Last Name of the registered user
         */
        lastName?: string,
    }): CancelablePromise<IUserToken> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/register_user',
            query: {
                'email': email,
                'first_names': firstNames,
                'last_name': lastName,
            },
        });
    }

}
