/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { ImReportId } from '../models/ImReportId';
import type { IReport } from '../models/IReport';

import type { CancelablePromise } from '../core/CancelablePromise';
import { OpenAPI } from '../core/OpenAPI';
import { request as __request } from '../core/request';

export class CognovisRestReportService {

    /**
     * @returns IReport Report with data
     * @throws ApiError
     */
    public static getReport({
        reportId,
    }: {
        /**
         * Report for which we want to retrieve the data
         */
        reportId: ImReportId,
    }): CancelablePromise<IReport> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/report',
            query: {
                'report_id': reportId,
            },
        });
    }

    /**
     * Returns a list of reports a user has access to without the data
     * @returns IReport Array of reports without the columns or row information
     * @throws ApiError
     */
    public static getReports(): CancelablePromise<Array<IReport>> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/reports',
        });
    }

}
