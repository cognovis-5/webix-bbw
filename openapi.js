// Requires
const { deepStrictEqual } = require("assert");
const { exec } = require("child_process");
const fs = require("fs");
const http = require("http");

var myArgs = process.argv.slice(2);
const api_key = myArgs[0];
var config = require("./env.json");
const baseURL = config.SERVER_URL;
const restURL = baseURL + "/cognovis-rest"
const openApiUrl = "/openapi?api_key=" + api_key;

const fullpath = "./openapi.yaml";

const npmGeneratorCommand =
    "npx openapi-typescript-codegen --input ./openapi.yaml --output ./sources/openapi  --useOptions";

const fullOpenApiUrl = restURL + openApiUrl;
saveOpenApiJSON(fullOpenApiUrl);

function saveOpenApiJSON(url) {
    http.get(url, function (res) {
        let content = "";
        res.on("data", function (chunk) {
            content += chunk;
        });
        res.on("end", function () {
            fs.writeFile(fullpath, content, function (err) {
                if (err) {
                    return console.log(err);
                } else {
                    console.log("\x1b[32m", fullpath + " was saved !", "\x1b[0m");
                    executeGenerator(npmGeneratorCommand);
                    // Need to copy over OpenAPI.ts as we are relying on it for working
                    // Endpoint to be based of the config and not what the openapi.yaml
                    // provided as the server
                    setTimeout(function () {
                        let changedData = '';
                        fs.readFile('./sources/openapi/core/OpenAPI.ts', 'utf8', function(err, data){
                            if (err) throw err;
                            changedData = "import { config } from \"../../config\";\n" + data.replace("'"+ restURL + "'",'config.restUrl')
                            fs.writeFile('./sources/openapi/core/OpenAPI.ts', changedData, (err) => {
                                if (err) throw err;
                            });
                        });
                        
                    }, 3500);
                }
            });
        });
    });
}

function executeGenerator(command) {
    console.log("Executing command: " + command);
    exec(command, (err, stdout, stderr) => {
        if(err) {
            console.log("Error: openapi was not generated !");
            console.log(err);
        } else {
            console.log("sources/openapi generated!");
        }
    });
    
}