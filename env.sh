#!/bin/sh
# line endings must be \n, not \r\n !
echo "window._env_ = {" > ./codebase/env-config.js
awk -F '=' '{ print $1 ": \"" (ENVIRON[$1] ? ENVIRON[$1] : $2) "\"," }' ./.env >> ./codebase/env-config.js
echo "}" >> ./codebase/env-config.js

echo "{" > ./env.json
awk -F '=' '{ print "\"" $1 "\"" ": \"" (ENVIRON[$1] ? ENVIRON[$1] : $2) "\"," }' ./.env >> ./env.json
echo "\"envCreated\":true}" >> ./env.json