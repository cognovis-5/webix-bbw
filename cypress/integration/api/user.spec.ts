import { matches } from "cypress/types/lodash";
import { OpenAPI,CognovisRestCompanyService, ICompany, ICompanyContact, IInternalCompany, IntranetCompanyStatus, IntranetCompanyType, IntranetPaymentTerm, IntranetSalutation, CognovisRestService, CognovisRestSystemService} from "../../../sources/openapi";

describe("Testing party creation", () => {
    it("Should create a party", function () {
        cy.wrap(CognovisRestSystemService.postRegisterParty({
            email: "test1@malte.sussdorff.de"
        }))
    })

    it("Should createa a person", function () {
        cy.wrap(CognovisRestSystemService.postRegisterParty({
            email: "test2@malte.sussdorff.de",
            firstNames: "Malte",
            lastName: "Sussdorff"
        }))
    })
})
