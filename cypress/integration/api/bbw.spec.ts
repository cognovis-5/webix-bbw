import { DanceRole, IBbwRegistration, IntranetCustBbwRestService, OpenAPI } from "../../../sources/openapi/"
import { randomizedName } from "../../support/helpers"

before(() => {
    cy.fixture('cypress.env.json')
    .then((testEnv) => {
        // First we authorize to our app
        OpenAPI.TOKEN = testEnv.enviromentTestVariables.DEV.admin.api_key;
    });
});

describe("Registration Tests", () => {
    it("Register a new user", function () {
        cy.wrap(IntranetCustBbwRestService.postBbwRegistration({
            requestBody: {
                first_names: randomizedName('First+'),
                last_name: randomizedName('Last+'),
                email: randomizedName('mail+') + '@malte.sussdorff.de',
                ha_city: 'hamburg',
                ha_country_code: 'de',
                ha_line1: randomizedName('street+'),
                ha_postal_code: '124109',
                course_material_id: 12266, // Workshop
                party_pass_interested_p: true,
                event_participant_type_id: DanceRole.LEAD
            }
        })).as('registration')
        cy.get('@registration').then(_registration => {
            const registration = _registration as unknown as IBbwRegistration
            expect(registration.ha_city).eq('hamburg')
            expect(registration.course_material_id).eq(12266)
            expect(registration.participant.user.id).not.eq(624)
        })
    }) 
    it("Register a known user", function () {
        cy.wrap(IntranetCustBbwRestService.postBbwRegistration({
            requestBody: {
                first_names: randomizedName('First+'),
                last_name: randomizedName('Last+'),
                email: 'malte.sussdorff@cognovis.de',
                ha_city: 'hamburg',
                ha_country_code: 'de',
                ha_line1: randomizedName('street+'),
                ha_postal_code: '124109',
                course_material_id: 12266, // Workshop
                party_pass_interested_p: true,
                event_participant_type_id: DanceRole.LEAD
            }
        })).as('registration')
        cy.get('@registration').then(_registration => {
            const registration = _registration as unknown as IBbwRegistration
            expect(registration.ha_city).eq('hamburg')
            expect(registration.course_material_id).eq(12266)
            expect(registration.email).eq('malte.sussdorff@cognovis.de')
            expect(registration.participant.user.id).eq(624)
        })
    }) 

})