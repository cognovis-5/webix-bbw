export function randomizedName (name:string):string {
    const randomNumber = Math.floor((9999 - 1) * Math.random());
    const randomizedTaskName = `${name}_${randomNumber}`;
    return randomizedTaskName;
}

export function getRandomNumberBetween(min:number, max:number):number {
    return Math.floor(Math.random() * (max - min + 1) + min);
}

export function getRandomName(length:number):string {
    let result           = '';
    const characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    const charactersLength = characters.length;
    for ( let i = 0; i < length; i++ ) {
       result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
 }

import { CognovisRestCompanyService, CognovisRestSystemService, IUserToken, OpenAPI } from "../../sources/openapi";

export function setUser(userId: number):string {

    cy.fixture('cypress.env.json')
    .then((testEnv) => {
        OpenAPI.TOKEN = testEnv.enviromentTestVariables.DEV.admin.api_key;
    })
    let userToken: string
    CognovisRestSystemService.getUserToken({
        userId: userId
    }).then(_contactUser => {
        const contactUser = _contactUser as unknown as IUserToken
        console.log('Bearer new ' + contactUser.user_id + ' ' + OpenAPI.TOKEN)
        userToken = contactUser.bearer_token
    })
    return userToken
}