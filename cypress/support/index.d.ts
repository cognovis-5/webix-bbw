/* eslint-disable @typescript-eslint/naming-convention */


// in cypress/support/index.d.ts
// load type definitions that come with Cypress module
/// <reference types="cypress" />

declare namespace Cypress {
    interface Chainable {
        loginToApp(username:string, password:string): Cypress.Chainable<void>;
    }
}

