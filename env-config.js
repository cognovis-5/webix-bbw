window._env_ = {
    URL: "http://localhost:8191/", 
    DEV_URL: "http://localhost:8191/",
    LOGO_URL: "http://cognovis.de/wp-content/uploads/2017/12/logo_cognovis-transparent.png",
    WEBIX_PORTAL_PATH: "/webix-portal"
};