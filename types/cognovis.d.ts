/* eslint-disable @typescript-eslint/no-explicit-any */

interface IBuildsDynfieldCreationModal {
    afterShow:() => void;
    submit:() => void;
    afterSubmit:<T>(object:T) => void;
}

interface ISidemenuItem {
    id: string;
    value: string;
    icon: string;
}

interface IWebixDatatableRow {
    column: string;
    row: string;
    start?: string;
    index?: number;
}

interface IDynfieldSetup {
    pages:{
        objectType:string, 
        pageUrl:string, 
        objectSubtypeId?:number
    }[],
    checkForRequiredFields?:boolean;
}

interface ICompany {
    object_name: string;
    company_name: string;
    address_line1: string;
    address_postal_code: string;
    address_city: string;
    address_country_code: string;
}

interface IQuote {
    cost_id: number;
    cost_type_id: number;
    cost_status_id: number;
    cost_name: string;
    amount: number;
    vat: number;
    currency: string;
}

interface IMaterial {
    id: number;
    name: string;
}

interface ICognovisPleaseWaitWindowParams {
    message: string;
}

interface ICognovisWindowOkButtonParams {
    message:string;
    okButtonText:string; 
    width?:number;
    height?:number;
}


interface ICognovisServerResponse {
    json(): any;
}

interface IGenericGetterColumn {
    column: string;
}

interface ICategory {
    id: number;
    category_id: number;
    category_name: string;
    object_name: string;
    category_translated: string;
}

interface IUploadedFile {
    filename: string;
    path: string;
    type?: string;
}

interface ILeftNavigationItem {
    name?: string;
    label?: string;
    url?: string;
    menu_id?: number;
    value: string;
    id: string;
    icon: string;
    parent_menu_id?: number;
    data?: ILeftNavigationItem[];
    urlParam?:string | number;
}

interface ICombobox {
    id: number | string;
    value: number | string;
    aux_int1?:number;
    aux_string1?:string;
}

interface ICognovisCredentials {
    format: string;
    user_id: string;
    auto_login: string;
}

interface IGetterCompanies {
    columns: IGenericGetterColumn[];
    //IntranetCompanyType: IntranetCompanyType;
}

interface IAdditionalParams {
    columns: IGenericGetterColumn;
}

interface ICompany {
    company_name: string;
    //company_type_id: IntranetCompanyType;
}


interface IKeyAccountInfo {
    key_account_first_names: string;
    key_account_last_name: string;
    key_account_email: string;
    key_account_work_phone: string;
    logo_url: string;
    favicon_url: string;
}

interface IKeyValuePair {
    key: string;
    value: string;
}

interface ICognovisComboOptions {
    id: number | string;
    value: number | string;
}


interface ICognovisInformationBoxConfig {
    entityId:number;
    webixId:string;
    title:string;
    editModalTitle?:string;
    fields:ICognovisInformationBoxField[];
    saveAction(data)
}

interface ICognovisInformationBoxFieldConfig {
    type:string;
    options?:any;
    url?:any;
    onChange?(data)
    onAfterRender?
}

interface ICognovisInformationBoxField {
    id?:string;
    name:string;
    label?:string;
    value?:string | any;
    valueKey?:string;
    translationKey?:string;
    editConfig:ICognovisInformationBoxFieldConfig | boolean;
}


interface ICognovisPreliminaryCostsSummaryData {
    quotes:{
        totalNumber:number;
        totalAmount:number;
    },
    purchaseOrders: {
        totalNumber:number;
        totalAmount:number;     
    },
    timeTracking: {
        totalNumber:number;
        totalAmount:number;
    },
    otherBudget: {
        totalNumber:number;
        totalAmount:number;
    },
    totalNumberSummary:number;
    totalCosts:number;
    totalAmountSummary:number;
}

interface ICognovisActualCostsSummaryData {
    invoices:{
        totalNumber:number;
        totalAmount:number;
    },
    creditNotes: {
        totalNumber:number;
        totalAmount:number;     
    },
    timeTracking: {
        totalNumber:number;
        totalAmount:number;
    },
    otherCosts: {
        totalNumber:number;
        totalAmount:number;
    },
    totalNumberSummary:number;
    totalCosts:number;
    totalAmountSummary:number;
}

interface ICognovisSendEmailModal {
    sender:string;
    recipient:string;
    subject:string;
    content?:string;
}

interface ICognovisGrouppedFreelancePackages<T> {
    packageName:string;
    packageTaskNames:string;
    packages:ICognovisGrouppedFreelancePackage<T>[];
    grouppedByType:any;
    targetLanguageId:number;
    targetLanguageName:string;
}

interface ICognovisGrouppedFreelancePackage<T> {
    packageId:number;
    taskIdsStringified:string;
    freelancePackage:T;
}

interface ICognovisPontentialAssignment {
    freelancePackageId:number;
    freelancerIds:string;
    deadline:string;
    rate:number;
    uomId:number;
    units:number;
}

interface ICognovisNewFreelancePackage {
    id:number;
    name:string;
    tasks?: Array<{
        trans_task: {
            id:number
        }
    }>;
    newOne:boolean;
}

interface ICognovisModalAdditionlData {
    assignment?:any;
}

interface Window {
    _env_?: ICognovisEnvironment;
}

interface ICognovisEnvironment {
    SERVER_URL: string,
    PORTAL_URL: string,
    LOGO_URL: string,
    WEBIX_PORTAL_PATH: string
}

interface ICognovisFormFieldConfig {
    name:string,
    hidden:boolean,
    editable:boolean,
    value?:string | number
}

type IMainTranslatorsPortletMode = "user" | "im_company";